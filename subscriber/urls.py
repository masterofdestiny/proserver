from django.conf.urls import patterns, include, url
from provising import  settings
# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()
from subscriber import views
urlpatterns = patterns('',
    # Examples:
    url(r'^$', views.home, name='subdcriber_home'),
    url(r'^registration/$', views.subscriberregistration, name='subscriber_registration'),
    url(r'^edit/$', views.edit, name='subscriber_edit'),
    url(r'^print/$', views.printp, name='subscriber_print'),
    url(r'^printcdr/$', views.printcdr, name='subscriber_printcdr'),
    url(r'^generatecsvcdr/$', views.generate_csv, name='cdr_generate_csv'),
    url(r'^prospectgeneratecsv/$', views.prospect_generate_csv, name='prospect_generate_csv'),

    url(r'^subdcriberlogout/$', views.subdcriber_logout, name='subdcriber_logout'),


    url(r'^login/$', views.subscriberlogin, name='subscriber_login'),
    url(r'^reports/$', views.subscriberreport, name='subscriber_report'),
    url(r'^agents/$', views.subscriberagent, name='subscriber_agents'), 
    url(r'^agents/details/$', views.getagent_detail, name='getagent_detail'), 
    url(r'^products/$', views.subscriber_product, name='subscriber_product'), 
    url(r'^toolkit/$', views.subscriber_toolkit, name='subscriber_toolkit'), 
    url(r'^settings/$', views.subscriber_print, name='subscriber_print'), 
    url(r'^getaboutagent/$', views.getaboutagent, name='getaboutagent'),
    url(r'^product/add/$', views.add_product, name='add_product'),
    url(r'^products/assign/$', views.assign_product, name='assign_product'),
   

    url(r'^products/edit/$', views.edit_product, name='edit_product'),
    url(r'^products/delete/$', views.delete_product, name='delete_product'),


    url(r'^getfiles/$', views.getfiles, name='getfiles'), 
    url(r'^searchcdr/$', views.searchcdr, name='searchcdr'),
    url(r'^agents/password/$', views.resetpassword, name='pchange'),
    url(r'^searchprospect/$', views.searchprospect, name='searchprospect'),
   
    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
)
