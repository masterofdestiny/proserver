import hashlib 
import datetime
import requests
import json


class RestRequest:
        
    def __init__(self):
        self.url = None
        self.error = None
        self.response = None
        getit =  datetime.datetime.utcnow() - datetime.timedelta(minutes = 1)
        self.get_timestamp = getit.strftime("%Y%m%d%H%M%S")

    def POST(self, params, subscriber_id, rest_url,apikey, secretstring):
        self.response =  self.REST('POST', params, subscriber_id, rest_url,apikey, secretstring)

    def PUT(self, params, subscriber_id, rest_url,apikey, secretstring):
        self.REST('PUT', params, subscriber_id, rest_url,apikey,secretstring)

    def DELETE(self, params, subscriber_id, rest_url,apikey,secretstring):
        self.REST('DELETE', params, subscriber_id,rest_url,apikey,secretstring)

    def GET(self,rest_url,apikey,secretstring):
        self.response = self.GETT('GET', rest_url,apikey,secretstring)
        

    def REST(self, method,params,subscriber_id,rest_url,apikey,secretstring):
        if method == 'POST':
            if type(params) != dict:
                return False
            else:
                get_sorted_diect  = params
                generatestring = ''
                for x,v in sorted(get_sorted_diect.items()):
                    generatestring +=  x
                    generatestring += '='
                    generatestring +=  v          
                customer_id = subscriber_id
                x_timestamp = self.get_timestamp
                x_ucs_version = '1.0'
                hashit =  x_timestamp + secretstring + generatestring
                getit  = hashlib.sha1(hashit).hexdigest()    
                x_ucs_auth_key = getit
                x_ucs_api_key = apikey 
                headers = {'content-type': 'application/json; charset=UTF-8','Accept':'application/json; charset=UTF-8','x-ucs-auth-key':getit
                           ,'x-ucs-version':x_ucs_version,'x-ucs-api-key':x_ucs_api_key,'x-timestamp':x_timestamp,'customer_id':'caason'
                           }
                print json.dumps(params)
                self.response = requests.post(rest_url, data=json.dumps(params),headers = headers)
                print self.response
                return  self.response

            
        elif method == 'PUT':
            if type(params) != dict:
                return False
            else:
                get_sorted_diect  = sorted(params)
                generatestring = ''
                for x,v in get_sorted_diect.items():
                    generatestring +=  x
                    generatestring += '='
                    generatestring +=  v          
                #  Create timstamp 
                customer_id = subscriber_id
                x_timestamp = self.get_timestamp
                x_ucs_version = '1.0'
                hashit = x_timestamp + secretstring + generatestring
                getit  = hashlib.sha1(hashit).hexdigest()    
                x_ucs_auth_key = getit
                x_ucs_api_key = 'Something'
                headers = {'content-type': 'application/json; charset=UTF-8','Accept':'application/json; charset=UTF-8','x-ucs-auth-key':getit
                           ,'x-ucs-version':x_ucs_version,'x-ucs-api-key':x_ucs_api_key,'x-timestamp':x_timestamp,'customer_id':'caason'
                           }

                self.response = requests.put(rest_url, data=json.dumps(params),headers = headers)            
                return  self.response
        elif method == 'DELETE':
            if type(params) != dict:
                return False
            else:
                get_sorted_diect  = sorted(params)
                generatestring = ''
                for x,v in get_sorted_diect.items():
                    generatestring +  x
                    generatestring + '='
                    generatestring +  v          
                #  Create timstamp 
                customer_id = subscriber_id
                x_timestamp = self.get_timestamp
                x_ucs_version = '1.0'
                hashit = x_timestamp  + secretstring
                getit  = hashlib.sha1(hashit).hexdigest()    
                x_ucs_auth_key = getit
                x_ucs_api_key =  apikey
                headers = {'content-type': 'application/json; charset=UTF-8','Accept':'application/json; charset=UTF-8','x-ucs-auth-key':getit
                           ,'x-ucs-version':x_ucs_version,'x-ucs-api-key':x_ucs_api_key,'x-timestamp':x_timestamp,'customer_id':'caason'
                           }
                self.response = requests.delete(rest_url, data=json.dumps(params),headers = headers)            
                return  self.response

    
    
    

    def GETT(self,method,rest_url,apikey,secretstring):
         if method == "GET":
            x_timestamp = self.get_timestamp
            x_ucs_version = '1.0'
            hashit = x_timestamp + secretstring 
            getit  = hashlib.sha1(hashit).hexdigest()    
            x_ucs_auth_key = getit
            x_ucs_api_key = apikey
            headers = {'content-type': 'application/json; charset=UTF-8','Accept':'application/json; charset=UTF-8','x-ucs-auth-key':getit
                      ,'x-ucs-version':x_ucs_version,'x-ucs-api-key':x_ucs_api_key,'x-timestamp':x_timestamp,'customer_id':'caason'
                      }


            self.response = requests.get(rest_url,headers = headers)            
            return  self.response   
                
                
                
class Agent:
    def __init__(self):
        self.url = None
        self.error = None
        self.response = None
        getit =  datetime.datetime.utcnow() - datetime.timedelta(minutes = 2)
        self.get_timestamp = getit.strftime("%Y%m%d%H%M%S")
        # datetime.datetime.utcnow().strftime("%Y%m%d%H%M%S")

    def POST(self, params,subscriber_id,rest_url,apikey, secretstring):
        if type(params) != dict:
            return False
        else:
            get_sorted_diect  = params
            generatestring = ''
            for x,v in sorted(get_sorted_diect.items()):
                generatestring +=  x
                generatestring += '='
                generatestring +=  v
            x_timestamp = self.get_timestamp
            x_ucs_version = '1.0'
            hashit =  x_timestamp + secretstring + generatestring
            getit  = hashlib.sha1(hashit).hexdigest()
            x_ucs_auth_key = getit
            x_ucs_api_key = apikey
            headers = {'content-type': 'application/json; charset=UTF-8','Accept':'application/json; charset=UTF-8','x-ucs-auth-key':getit
                       ,'x-ucs-version':x_ucs_version,'x-ucs-api-key':x_ucs_api_key,'x-timestamp':x_timestamp,'customer_id':'caason'
                       }
            # print json.dumps(params)
            # print headers
            # print rest_url
            self.response = requests.post(rest_url, data=json.dumps(params),headers = headers)
            return  self.response




    def GETT(self,method,rest_url,apikey,secretstring):
            x_timestamp = self.get_timestamp
            x_ucs_version = '1.0'
            hashit = x_timestamp + secretstring
            getit  = hashlib.sha1(hashit).hexdigest()
            x_ucs_auth_key = getit
            x_ucs_api_key = apikey
            headers = {'content-type': 'application/json; charset=UTF-8','Accept':'application/json; charset=UTF-8','x-ucs-auth-key':getit
                      ,'x-ucs-version':x_ucs_version,'x-ucs-api-key':x_ucs_api_key,'x-timestamp':x_timestamp,'customer_id':'caason'
                      }
            self.response = requests.get(rest_url,headers = headers)
            return  self.response



    def PUT(self, params,subscriber_id,rest_url,apikey, secretstring):
        if type(params) != dict:
            return False
        else:
            get_sorted_diect  = params
            generatestring = ''
            for x,v in sorted(get_sorted_diect.items()):
                generatestring +=  x
                generatestring += '='
                generatestring +=  v
            x_timestamp = self.get_timestamp
            x_ucs_version = '1.0'
            hashit =  x_timestamp + secretstring + generatestring
            getit  = hashlib.sha1(hashit).hexdigest()
            x_ucs_auth_key = getit
            x_ucs_api_key = apikey
            headers = {'content-type': 'application/json; charset=UTF-8','Accept':'application/json; charset=UTF-8','x-ucs-auth-key':getit
                   ,'x-ucs-version':x_ucs_version,'x-ucs-api-key':x_ucs_api_key,'x-timestamp':x_timestamp,'customer_id':'caason'
                       }
            print json.dumps(params)
            self.response = requests.put(rest_url, data=json.dumps(params),headers = headers)
            return  self.response



    def DELETE(self,method,rest_url,apikey,secretstring):
        x_timestamp = self.get_timestamp
        x_ucs_version = '1.0'
        hashit = x_timestamp + secretstring
        getit  = hashlib.sha1(hashit).hexdigest()
        x_ucs_auth_key = getit
        x_ucs_api_key = apikey
        headers = {'content-type': 'application/json; charset=UTF-8','Accept':'application/json; charset=UTF-8','x-ucs-auth-key':getit
                  ,'x-ucs-version':x_ucs_version,'x-ucs-api-key':x_ucs_api_key,'x-timestamp':x_timestamp,'customer_id':'caason'
                  }
        self.response = requests.delete(rest_url,headers = headers)
        return  self.response


    def GETIMAGE(self,method,rest_url,apikey,secretstring):
        x_timestamp = self.get_timestamp
        x_ucs_version = '1.0'
        hashit = x_timestamp + secretstring
        getit  = hashlib.sha1(hashit).hexdigest()
        x_ucs_auth_key = getit
        x_ucs_api_key = apikey
        headers = {'content-type': 'application/json; charset=UTF-8','Accept':'image/png;','x-ucs-auth-key':getit
                  ,'x-ucs-version':x_ucs_version,'x-ucs-api-key':x_ucs_api_key,'x-timestamp':x_timestamp,'customer_id':'caason'
                  }
        self.response = requests.get(rest_url,headers = headers)
        return  self.response

    def POST2GetImage(self, params,subscriber_id,rest_url,apikey, secretstring):
        if type(params) != dict:
            return False
        else:
            get_sorted_diect  = params
            generatestring = ''
            for x,v in sorted(get_sorted_diect.items()):
                generatestring +=  x
                generatestring += '='
                generatestring +=  v
            x_timestamp = self.get_timestamp
            x_ucs_version = '1.0'
            hashit =  x_timestamp + secretstring + generatestring
            getit  = hashlib.sha1(hashit).hexdigest()
            x_ucs_auth_key = getit
            x_ucs_api_key = apikey
            headers = {'content-type': 'application/json; charset=UTF-8','Accept':'image/png','x-ucs-auth-key':getit
                       ,'x-ucs-version':x_ucs_version,'x-ucs-api-key':x_ucs_api_key,'x-timestamp':x_timestamp,'customer_id':'caason'
                       }
            # print json.dumps(params)
            # print headers
            # print rest_url
            self.response = requests.post(rest_url, data=json.dumps(params),headers = headers)
            return  self.response


