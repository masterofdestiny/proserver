import datetime
from django.http import Http404, HttpResponseRedirect
from django.shortcuts import render_to_response, get_object_or_404
from django.template import RequestContext
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.utils.translation import ugettext as _
from django.utils.translation import ugettext_noop
from django.core.urlresolvers import reverse
from django.conf import settings
from rest import *
from provising.settings import BASE_URL
from django.contrib import messages

from datetime import datetime


def logout(request):
        del request.session['extensionId']

	messages.success(request, 'you are successfully logged out')
	return HttpResponseRedirect('/agents/login/')


def home(request):
    try: request.session['extensionId']
    except: return HttpResponseRedirect('/agents/login/')
    extensionId = request.session['extensionId'] 
    REST_URL = BASE_URL+'/UCSRestService/prospectrequest/' +str(extensionId) +'/extension'
    context = {}
    geta = Agent()
    getr = geta.GETT('caason',REST_URL,'apikey','SecretString')
    geti = geta.response.text
    getwj = json.loads(geti)
    
    for x,v in getwj.items():
        if x == 'data':
             context['extension'] = v

    RESTC_URL = BASE_URL+'/UCSRestService/calldetailrecord/' + str(extensionId) +'/extension'
    getac = Agent()
    getrc = getac.GETT('caason',RESTC_URL,'apikey','SecretString')
    getic = getac.response.text
    getwjc = json.loads(getic)
    for x,v in getwjc.items():
        if x == 'data':
             context['cdr'] = v

    RESTP_URL = BASE_URL+'/UCSRestService/extension/' +str(extensionId)
    geta = Agent()
    getr = geta.GETT('caason',RESTP_URL,'apikey','SecretString')
    geti = geta.response.text
    getwj = json.loads(geti)
    for x,v in getwj.items():
        if x == 'data':
             context['agent'] = v
    print context['agent']





    return render_to_response('agent/reports.html', context, context_instance=RequestContext(request))
    
def agentprofile(request):

    try: request.session['extensionId']
    except: return HttpResponseRedirect('/agents/login/')
    extensionId = request.session['extensionId'] 
    REST_URL = BASE_URL+'/UCSRestService/extension/' +str(extensionId)
    context = {}
    geta = Agent()
    getr = geta.GETT('caason',REST_URL,'apikey','SecretString')
    geti = geta.response.text
    getwj = json.loads(geti)
    for x,v in getwj.items():
        if x == 'data':
             context['agent'] = v
    if request.method == "POST":

        params = {}
        params['extensionId'] = str(extensionId)
        params['name'] = request.POST['name'].encode('utf-8')
        params['contactTitle'] = request.POST['contactTitle'].encode('utf-8')
        params['contactAddress'] = request.POST['contactAddress'].encode('utf-8')
        params['contactCity'] = request.POST['contactCity'].encode('utf-8')

        params['contactState'] = request.POST['contactState'].encode('utf-8')
        params['contactZIPCode'] = request.POST['contactZIPCode'].encode('utf-8')
        params['contactCountry'] = request.POST['contactCountry'].encode('utf-8')
        params['ContactEmail1'] = request.POST['ContactEmail1'].encode('utf-8')


        params['ContactEmail2'] = request.POST['ContactEmail2'].encode('utf-8')
        params['contactPhone1'] = request.POST['contactPhone1'].encode('utf-8')
        params['contactPhone2'] = request.POST['contactPhone2'].encode('utf-8')
     #   params['contactPhone3'] = request.POST['contactPhone3'].encode('utf-8')
        params['contactMobile'] = request.POST['contactMobile'].encode('utf-8')
        params['customInfo'] = request.POST['customInfo'].encode('utf-8')
        params['readyToAcceptCalls'] = request.POST['readyToAcceptCalls'].encode('utf-8')
        params['groupName'] = request.POST['groupName'].encode('utf-8')
        print params
        REST_URL = BASE_URL +'/UCSRestService/extension'
        geta = Agent()
        getr = geta.PUT(params,'caason',REST_URL,'apikey','SecretString')
        geti = geta.response.text
        getwj = json.loads(geti)
	print getwj
        for x,v in getwj.items():
                if x == 'status':
                    if v == 200:
                          return HttpResponseRedirect('/agents/')

    return render_to_response('agent/agentprofile.html', context, context_instance=RequestContext(request))

def agentlogin(request):



    context = {}
    if request.method == 'POST':
        get_user_name  = request.POST['login'].encode('utf-8')
        get_user_password = request.POST['pass'].encode('utf-8')
        params = {}
        params['userName'] = get_user_name
        params['password'] = get_user_password
        REST_URL = BASE_URL +'/UCSRestService/login/extension'
        getclass = Agent()
        getr = getclass.POST(params,'caason',REST_URL,'apikey','SecretString')
        gi = getclass.response.text
        getdict = json.loads(gi)
        print getdict
        for x,v in getdict.items():
                if x =="data":
                   for xx in v:
 		      try:
			 request.session['extensionId'] = xx['extensionId']
                      	 return HttpResponseRedirect('/agents/')
		      except:
			messages.error(request, 'Your name or password is not correct.')
			return HttpResponseRedirect('/agents/login')
    
    return render_to_response('agent/agentlogin.html', context, context_instance=RequestContext(request))

def agentsearch(request):

    try: request.session['extensionId']
    except: return HttpResponseRedirect('/agents/login/')
    extensionId = request.session['extensionId']
    params = {}
    if request.method == "POST":
        REST_URL = BASE_URL +'/UCSRestService/report/prospectrequests/extension'
        context = {}
        geta = Agent()
        getstart = request.POST['get_start']
        getend =  request.POST['get_end']
        get_start =    datetime.strptime(getstart,'%m/%d/%Y')
        get_end = datetime.strptime(getend,'%m/%d/%Y')
        get_start = str(get_start)
        get_end = str(get_end)
        get_start = filter(lambda x: x.isdigit(),get_start)
        get_end = filter(lambda x: x.isdigit(),get_end)
        params['fromDate'] = get_start
        params['toDate'] =   get_end
        params['extensionId'] = str(extensionId)

        getr = geta.POST(params,'caason',REST_URL,'apikey','SecretString')
        geti = geta.response.text
        getw= json.loads(geti)



#	"""
#        get_start =    datetime.strptime(getstart,'%m/%d/%Y')
 #       get_end = datetime.strptime(getend,'%m/%d/%Y')
 #       get_start = str(get_start)
#        get_end = str(get_end)
#        get_start = filter(lambda x: x.isdigit(),get_start)
#        get_end = filter(lambda x: x.isdigit(),get_end)
#	print get_start
#	print "+++++++++++++++++++++++"
#	print get_end
#	print "_____+++++++++++++++++++++++++++++++++++++++++++"
#        params['fromDate'] = str(get_start)
#        params['toDate'] =   str(get_end)
#        params['extensionId'] = extensionId
        getr = geta.POST(params,'caason',REST_URL,'apikey','SecretString')
        geti = geta.response.text
        getw= json.loads(geti)
     	print getw
        for i,j in getw.items():
            if i == "data":
                getit = j
                context['extension'] = getit

        RESTC_URL = BASE_URL +'/UCSRestService/calldetailrecord/' +str(extensionId)+'/extension'
        getac = Agent()
        getrc = getac.GETT('caason',RESTC_URL,'apikey','SecretString')
        getic = getac.response.text
        getwjc = json.loads(getic)
        for k,v in getwjc.items():
           if k == "data":
              context['cdr'] = v
        context['report'] = True
    else:
       context = {}	
       REST_URL = BASE_URL +'/UCSRestService/prospectrequest/' +str(extensionId) +'/extension'
       geta = Agent()
       getr = geta.GETT('caason',REST_URL,'apikey','SecretString')
       geti = geta.response.text
       getwj = json.loads(geti)
       for x,v in getwj.items():
           if x == 'data':
               context['extension'] = v
       RESTC_URL = BASE_URL +'/UCSRestService/calldetailrecord/' +str(extensionId) +'/extension'
       getac = Agent()
       getrc = getac.GETT('caason',RESTC_URL,'apikey','SecretString')
       getic = getac.response.text
       getwjc = json.loads(getic)
       print getwjc
       for x,v in getwjc.items():
           if x == 'data':
             context['cdr'] = v

    return render_to_response('agent/reports.html', context, context_instance=RequestContext(request))
    
def  agentsearchcdr(request):
    try: request.session['extensionId']
    except: return HttpResponseRedirect('/agents/login/')
    extensionId = request.session['extensionId']

    params = {}
    if request.method == "POST":
        #REST_URL = 'http://10.10.10.76:8100/UCSRestService/prospectrequest/' +str('306') +'/extension'
        #context = {}
        #geta = Agent()
        #getr = geta.GETT('caason',REST_URL,'apikey','SecretString')
        #geti = geta.response.text
        #getwj = json.loads(geti)
        #for x,v in getwj.items():
         #   if x == 'data':
         #      context['extension'] = v
        RESTC_URL = BASE_URL +'/UCSRestService/calldetailrecord/' +str(extensionId) +'/extension'
        getac = Agent()

        getstart = request.POST['get_start']
        getend =  request.POST['get_end']
        from datetime import datetime
        get_start =    datetime.strptime(getstart,'%m/%d/%Y')
        get_end = datetime.strptime(getend,'%m/%d/%Y')
        get_start = str(get_start)
        get_end = str(get_end)
        get_start = filter(lambda x: x.isdigit(),get_start)
        get_end = filter(lambda x: x.isdigit(),get_end)
        params['fromDate'] = get_start
        params['toDate'] =   get_end
        params['extensionId'] = '306'

        getrc = getac.POST(params,'caason',RESTC_URL,'apikey','SecretString')
        print getrc
        getic = getac.response.text
        print "++++++++++++++++++++++"
        print getic
        getwjc = json.loads(getic)
        for k,v in getwjc.items():
           if k == "data":
              context['cdr'] = v
        context['report'] = True


    else:
       context = {}
       REST_URL = BASE_URL+ '/UCSRestService/prospectrequest/' +str(extensionId) +'/extension'
       geta = Agent()
       getr = geta.GETT('caason',REST_URL,'apikey','SecretString')
       geti = geta.response.text
       getwj = json.loads(geti)
       for x,v in getwj.items():
           if x == 'data':
               context['extension'] = v
       RESTC_URL = BASE_URL+'/UCSRestService/calldetailrecord/' +str(extensionId) +'/extension'
       getac = Agent()
       getrc = getac.GETT('caason',RESTC_URL,'apikey','SecretString')
       getic = getac.response.text
       getwjc = json.loads(getic)
       print getwjc
       for x,v in getwjc.items():
           if x == 'data':
             context['cdr'] = v

    return render_to_response('agent/reports.html', context, context_instance=RequestContext(request))

