from django.conf.urls import patterns, include, url
from provising import  settings
# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()
from agent import views
urlpatterns = patterns('',
    # Examples:
    url(r'^profile/$', views.agentprofile, name='agent_profile'),
    url(r'^login/$', views.agentlogin, name='agent_login'),
    url(r'^$', views.home, name='agent_report'),
    url(r'^searchprospect$', views.agentsearch, name='agentsearch'),
     url(r'^agentsearchcdr$', views.agentsearchcdr, name='agentsearchcdr'),
 url(r'^logout/$', views.logout, name='agent_logout'),



)
