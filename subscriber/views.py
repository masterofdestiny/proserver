# Create your views here.
import datetime
from django.http import Http404, HttpResponseRedirect
from django.shortcuts import render_to_response, get_object_or_404
from django.template import RequestContext
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.utils.translation import ugettext as _
from django.utils.translation import ugettext_noop
from django.core.urlresolvers import reverse
from django.conf import settings
from agent.rest import *
import json
from datetime import datetime
import csv
from django.http import HttpResponse
from django.core.mail import send_mail

from provising.settings import SITE_URL,WEBMASTER,BASE_URL,SUBJECT
from emailstring import *

from django.contrib import messages


def subdcriber_logout(request):
	del request.session['subscriberId']
	del request.session['defaultExtensionId']
	messages.info(request, 'You are successfully logged out.')
	return HttpResponseRedirect('/subscriber/login')

def generate_csv(request):
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="cdrreport.csv"'
    writer = csv.writer(response)
    subscriberId = request.session['subscriberId']
    REST_URL =  BASE_URL +'/UCSRestService/calldetailrecord/' + subscriberId + '/subscriber'
    context = {}
    geta = Agent()
    getr = geta.GETT('caason',REST_URL,'apikey','SecretString')
    geti = geta.response.text
    getwj = json.loads(geti)
    for i,j in getwj.items():
        if i == "data":
           for xx in j:
              xx['confStart'] = datetime.strptime(xx['confStart'], '%Y-%m-%d %H:%M:%S.%f')
              writer.writerow([xx['conferenceStatus'],xx['confStart'],xx['prospectNumber'],xx['productCode'],xx['initiationType'],xx['name'],xx['idCall']])
           getit = j
    return response

def prospect_generate_csv(request):
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="cdrreport.csv"'
    writer = csv.writer(response)
    subscriberId = request.session['subscriberId']
    REST_URL =  BASE_URL +'/UCSRestService/prospectrequest/' + subscriberId + '/subscriber'
    geta = Agent()
    getr = geta.GETT('caason',REST_URL,'apikey','SecretString')
    geti = geta.response.text
    getwj = json.loads(geti)
    for i,j in getwj.items():
        if i == "data":
           for xx in j:
              writer.writerow([xx['status'],xx['dateCreated'],xx['phone1'],xx['productCode'],xx['extensionId'],xx['name'],xx['message'],xx['trackId'],xx['email1']])
           getit = j
    return response

def printcdr(request):
    context = {}
    subscriberId = request.session['subscriberId']
    REST_URL =  BASE_URL +'/UCSRestService/calldetailrecord/' + subscriberId + '/subscriber'
    context = {}
    geta = Agent()
    getr = geta.GETT('caason',REST_URL,'apikey','SecretString')
    geti = geta.response.text
    getwj = json.loads(geti)
    for i,j in getwj.items():
        if i == "data":
           for xx in j:
              xx['confStart'] = datetime.strptime(xx['confStart'], '%Y-%m-%d %H:%M:%S.%f')
           getit = j
    context['hi'] = getit
    return render_to_response('actual/printcdr.html', context, context_instance=RequestContext(request))

def printp(request):
    context = {}
    subscriberId = request.session['subscriberId']
    REST_URL =  BASE_URL +'/UCSRestService/prospectrequest/' + subscriberId + '/subscriber'
    context = {}
    geta = Agent()
    getr = geta.GETT('caason',REST_URL,'apikey','SecretString')
    geti = geta.response.text
    getwj = json.loads(geti)
    for i,j in getwj.items():
        if i == "data":
           for xx in j:
              xx['dateCreated'] = datetime.strptime(xx['dateCreated'], '%Y-%m-%d %H:%M:%S.%f')
           getit = j
    context['hi'] = getit

    return render_to_response('actual/printreport.html', context, context_instance=RequestContext(request))

def edit(request):
    context = {}
    subscriberId = request.session['subscriberId']
    REST_URL =  BASE_URL +'/UCSRestService/subscriber/' + subscriberId
    getclass = RestRequest()
    getr = getclass.GET(REST_URL,'apikey','SecretString')
    gi = getclass.response.text
    getdict = json.loads(gi)
    print getdict
    for x,v in getdict.items():
        if x == 'data':
             context['subscriber'] = v
    if request.method == "POST":
	params = {}
        params['subscriberId'] =subscriberId
        getreseller = request.POST['reseller'].encode('utf-8')
        get_first_name = request.POST['first_name'].encode('utf-8')
        get_last_name = request.POST['last_name'].encode('utf-8')
        get_username = request.POST['username'].encode('utf-8')
#        get_mobile = request.POST['mobile'].encode('utf-8')
#        params['subscriberId']  = get_username
        params['customerId']  = 'caason'
	params['contactMiddleName'] = request.POST['contactMiddleName'].encode('utf-8')
	params['userName'] = get_username
	params['groupName'] = request.POST['groupName'].encode('utf-8')
        params['contactMobile1'] = request.POST['contactMobile1'].encode('utf-8')
        params['publishedEmailAddress'] = request.POST['publishedEmailAddress'].encode('utf-8')
	params['publishedSMSNumber'] = request.POST['publishedSMSNumber'].encode('utf-8')
	params['publishedPhoneNumber'] = request.POST['publishedPhoneNumber'].encode('utf-8')
        params['name']  = get_first_name + get_last_name
        params['contactFirstName'] = get_first_name
        params['contactLastName'] = get_last_name
 #       params['contactMobile']  = get_mobile
        params['contactEmail1']  = request.POST['contactEmailAddress'].encode('utf-8')
        REST_URL =  BASE_URL +'/UCSRestService/subscriber'
        getclass = Agent()
        getr = getclass.PUT(params,'caason',REST_URL,'apikey','SecretString')
        gi = getclass.response.text
        getdict = json.loads(gi)
	print getdict
        for x,v in getdict.items():
                if x == 'status':
                    if v == 200:
                          return HttpResponseRedirect('/subscriber/')
    return render_to_response('actual/editsubscriber.html', context, context_instance=RequestContext(request))




def delete_product(request):
   if request.method == "GET":
       context = {}
       getpid = request.GET['pid']
       RESTC_URL = BASE_URL + '/UCSRestService/product/' + getpid
       getac = Agent()
       getrc = getac.DELETE('caason',RESTC_URL,'apikey','SecretString')
       getic = getac.response.text
       getwjc = json.loads(getic)
       print getwjc
       for k,v in getwjc.items():
          if k == "status":
              if v == 200:
                return HttpResponseRedirect('/subscriber/products/')

def edit_product(request):
   context = {}
   if request.method == "GET":
        productId = request.GET['pid']
        RESTC_URL = BASE_URL + '/UCSRestService/product/' + productId  
        getac = Agent()
        getrc = getac.GETT('caason',RESTC_URL,'apikey','SecretString')
        getic = getac.response.text
        getwjc = json.loads(getic)
        for k,v in getwjc.items():
           if k == "data":
              context['cdr'] = v
        print context['cdr']
   if request.method == "POST":
        params = {}
        p_name = request.POST['name'].encode('utf-8')
        p_id = request.POST['productId'].encode('utf-8')
        p_code = request.POST['productCode'].encode('utf-8')
        p_description= request.POST['description'].encode('utf-8')
        p_extensionId = request.POST['extensionId'].encode('utf-8')
        params['name'] = p_name
        params['productCode'] = p_code
        params['productId'] = p_id
        params['description'] = p_description
        params['extensionId'] = p_extensionId
        REST_URL = BASE_URL + '/UCSRestService/product/' + p_id
        geta = Agent()
        getr = geta.PUT(params,'caason',REST_URL,'apikey','SecretString')
        geti = geta.response.text
        getwj = json.loads(geti)
        for x,v in getwj.items():
                if x == 'status':
                    if v == 200:
                          return HttpResponseRedirect('/subscriber/products/')
   return render_to_response('actual/editproduct.html', context, context_instance=RequestContext(request))


def searchprospect(request):
    context = {}
    subscriberId = request.session['subscriberId']
    params = {}
    if request.method == "POST":
    	REST_URL = BASE_URL +'/UCSRestService/report/prospectrequests/subscriber'
    	context = {}
        geta = Agent()
        getstart = request.POST['get_start']
        getend =  request.POST['get_end']
        from datetime import datetime
        get_start =    datetime.strptime(getstart,'%m/%d/%Y')
        get_end = datetime.strptime(getend,'%m/%d/%Y')
        get_start = str(get_start)
        get_end = str(get_end)
        get_start = filter(lambda x: x.isdigit(),get_start)
        get_end = filter(lambda x: x.isdigit(),get_end)
        params['fromDate'] = get_start
        params['toDate'] =   get_end
        params['subscriberId'] = subscriberId
        getr = geta.POST(params,'caason',REST_URL,'apikey','SecretString')
        geti = geta.response.text
        getw= json.loads(geti)
        for i,j in getw.items():
            if i == "data":
                getit = j
                context['hi'] = getit
        RESTC_URL = BASE_URL + '/UCSRestService/calldetailrecord/' + subscriberId + '/subscriber'
        getac = Agent()
        getrc = getac.GETT('caason',RESTC_URL,'apikey','SecretString')
        getic = getac.response.text
        getwjc = json.loads(getic)
        for k,v in getwjc.items():
           if k == "data":
              context['cdr'] = v
	context['report'] = True
    else:
        REST_URL = BASE_URL +'/UCSRestService/prospectrequest/' + subscriberId + '/subscriber'
        context = {}
        geta = Agent()
        getr = geta.GETT('caason',REST_URL,'apikey','SecretString')
        geti = geta.response.text
        getwj = json.loads(geti)
        for i,j in getwj.items():
            if i == "data":
                getit = j
        context['hi'] = getit

        RESTC_URL = BASE_URL +'/UCSRestService/calldetailrecord/' + subscriberId + '/subscriber'
        getac = Agent()
        getrc = getac.GETT('caason',RESTC_URL,'apikey','SecretString')
        getic = getac.response.text
        getwjc = json.loads(getic)
        for k,v in getwjc.items():
           if k == "data":
              context['cdr'] = v
        context['report'] = True
    return render_to_response('actual/reports.html', context, context_instance=RequestContext(request))




def assign_product(request):
    params = {}
    params['productId'] = request.POST['productId'].encode('utf-8')
    params['extensionId'] = request.POST['extensionId'].encode('utf-8')
    REST_URL = BASE_URL +'/UCSRestService/assignproduct'
    geta = Agent()
    getr = geta.PUT(params,'caason',REST_URL,'apikey','SecretString')
    geti = geta.response.text
    getwj = json.loads(geti)
    for x,v in getwj.items():
           if x == 'status':
               if v == 200:
                 return HttpResponseRedirect('/subscriber/products/')

def resetpassword(request):
    context = {}
    try:
	getid = request.GET['eid']
	context['geteid'] = getid
        getname = request.GET['name']
	context['name'] = getname
    except:pass
    if request.method == "POST":
	params = {}
	getid = request.POST['eid'].encode('utf-8')
        params['password'] = request.POST['password'].encode('utf-8')        
	params['name'] =  request.POST['name'].encode('utf-8')
  	params['subscriberId'] = request.session['subscriberId']
	eid = request.POST['eid'].encode('utf-8')
	params['extensionId'] = eid
        REST_URL = BASE_URL +'/UCSRestService/extension#/' + str(eid)   #('rohitdwivedi51@gmail.com')
        geta = Agent()
        getr = geta.PUT(params,'caason',REST_URL,'apikey','SecretString')
        geti = geta.response.text
        getwj = json.loads(geti)
        for x,v in getwj.items():
                if x == 'status':
                    if v == 200:
                          return HttpResponseRedirect('/subscriber/')

    return render_to_response('actual/changepassword.html', context, context_instance=RequestContext(request))

  
def home(request):
    try: request.session['subscriberId']
    except: return HttpResponseRedirect('/subscriber/login/')
    context = {}
    subscriberId = request.session['subscriberId']
    REST_URL = BASE_URL +'/UCSRestService/subscriber/' + subscriberId
    getclass = RestRequest()
    getr = getclass.GET(REST_URL,'apikey','SecretString')
    gi = getclass.response.text
    getdict = json.loads(gi)
    for x,v in getdict.items():
	if x == 'data':
		print v
	    	for xx  in v:
		    getdefault =  xx['defaultExtensionId']
		    getpnumber = xx['publishedPhoneNumber']
		    request.session['publishedPhoneNumber'] = getpnumber
		    request.session['defaultExtensionId'] = getdefault
         	    xy = request.session['subscriberId'] 
		context['subscribername'] = v	
    context['subscriberId'] = xy
    context['dashboard'] = True
    REST_URL = BASE_URL +'/UCSRestService/subscriber/' + subscriberId + '/extensions'
    getclass = RestRequest()
    getr = getclass.GET(REST_URL,'apikey','SecretString')
    gi = getclass.response.text
    getextensions =  json.loads(gi)
    for k,v in getextensions.items():
        if k == 'data':
  	    context['extensions'] =v

    RESTP_URL = BASE_URL +'/UCSRestService/subscriber/' + subscriberId + '/products'
    getclass = RestRequest()
    getr = getclass.GET(RESTP_URL,'apikey','SecretString')
    gi = getclass.response.text
    getproducts =  json.loads(gi)
    for k,v in getproducts.items():
            if  k == 'data':
               context['products'] =v
    return render_to_response('actual/index.html', context, context_instance=RequestContext(request))
    
def subscriberregistration(request):
    context = {}
    params = {}
    if request.method == 'GET':
	 getit = request.GET['rid']
         context['parameter']  = getit
    if request.method == 'POST':
        getreseller = request.POST['reseller'].encode('utf-8') 
        get_first_name = request.POST['first_name'].encode('utf-8') 
        get_last_name = request.POST['last_name'].encode('utf-8') 
        get_username = request.POST['username'].encode('utf-8') 
        get_password = request.POST['password'].encode('utf-8') 
        get_group_name = request.POST['groupn'].encode('utf-8') 
        get_mobile = request.POST['mobile'].encode('utf-8') 
        password = request.POST['password'].encode('utf-8')
        params['subscriberId']  = get_username
        params['customerId']  = 'caason'
        params['name']  = get_first_name + get_last_name
        params['contactFirstName'] = get_first_name
        params['contactLastName'] = get_last_name
        params['contactMobile1']  = get_mobile
        params['contactEmailAddress']  = get_username
        params['abnacnNumber']  = 'someabnacnnumbver'
        params['contactPhone1'] = get_mobile
	params['password']  = get_password
	params['userName'] = get_username

	REST_URL = BASE_URL +'/UCSRestService/subscriber'
        getclass = RestRequest()
        getr = getclass.POST(params,'caason',REST_URL,'apikey','SecretString')   
	gi = getclass.response.text        
	getdict = json.loads(gi)
	for x,v in getdict.items():
		if x == 'status':
		    if v == 200:
			  messages.success(request, 'Registration success')
	        	  return HttpResponseRedirect('/subscriber/login/')  
    return render_to_response('actual/subscriberregistration.html', context, context_instance=RequestContext(request))




def getaboutagent(request):

    try: request.session['subscriberId']
    except: return HttpResponseRedirect('/subscriber/login/')

    #customerId =  request.session['customerId']
    subscriberId = request.session['subscriberId']
    REST_URL = BASE_URL +'/UCSRestService/extension/' + str(subscriberId)
    context = {}
    geta = Agent()
    getr = geta.GETT('caason',REST_URL,'apikey','SecretString')
    geti = geta.response.text
    getwj = json.loads(geti)
    return render_to_response('actual/reports.html', context, context_instance=RequestContext(request))

def subscriberlogin(request):
    context = {}
    if request.method == 'POST':
        get_user_name  = request.POST['login'].encode('utf-8')
        get_user_password = request.POST['pass'].encode('utf-8')
	params = {}
        params['userName'] = get_user_name
	params['password'] = get_user_password    
        REST_URL = BASE_URL +'/UCSRestService/login/subscriber'
        getclass = RestRequest()
        getr = getclass.POST(params,'caason',REST_URL,'apikey','SecretString')
        gi = getclass.response.text
        getdict = json.loads(gi)
        print getdict
        for x,v in getdict.items():
                if x =="data":
	           for xx in v:
		      if xx['subscriberId'] != None:
			    request.session['subscriberId'] = xx['subscriberId']
   			    REST_URL = BASE_URL +'/UCSRestService/subscriber/' + xx['subscriberId']	
    			    getclass = RestRequest()
			    getr = getclass.GET(REST_URL,'apikey','SecretString')
    			    gi = getclass.response.text
    			    getdict = json.loads(gi)
    			    for y,z in getdict.items():
       			      if y == 'data':
	                        for xxy  in z:
		                    getpnumber = xxy['publishedPhoneNumber']
                		    request.session['publishedPhoneNumber'] = getpnumber

			    return HttpResponseRedirect('/subscriber/')
		if x == "error":
			messages.error(request, 'Invalid Username or password')			

    
    return render_to_response('actual/subscriberlogin.html', context, context_instance=RequestContext(request))

def  subscriberreport(request):
    try: request.session['subscriberId']
    except: return HttpResponseRedirect('/subscriber/login/')

    context = {}
    subscriberId = request.session['subscriberId']
    REST_URL = BASE_URL +'/UCSRestService/prospectrequest/' + subscriberId + '/subscriber'
    context = {}
    geta = Agent()
    getr = geta.GETT('caason',REST_URL,'apikey','SecretString')
    geti = geta.response.text
    getwj = json.loads(geti)
    print getwj
    for i,j in getwj.items():
        if i == "data":
	   for xx in j:
              xx['dateCreated'] = datetime.strptime(xx['dateCreated'], '%Y-%m-%d %H:%M:%S.%f')	
           getit = j
           context['hi'] = getit
    RESTC_URL = BASE_URL +'/UCSRestService/calldetailrecord/' + subscriberId + '/subscriber'
    getac = Agent()
    getrc = getac.GETT('caason',RESTC_URL,'apikey','SecretString')
    getic = getac.response.text
    getwjc = json.loads(getic)
    print "++++++++++++++++++++++++++++++++++++++++++++++++++"
    print getwjc
    for k,v in getwjc.items():
        if k == "data":
           for xx in v:
              xx['confStart'] = datetime.strptime(xx['confStart'], '%Y-%m-%d %H:%M:%S.%f')
	      context['cdr'] = v
    context['report'] = True
    return render_to_response('actual/reports.html', context, context_instance=RequestContext(request))


def searchcdr(request):
    context = {}
    subscriberId = request.session['subscriberId']
    params = {}
    if request.method == "POST":
    	REST_URL =  BASE_URL +'/UCSRestService/prospectrequest/'+ subscriberId + '/subscriber'
    	context = {}
    	geta = Agent()
    	getr = geta.GETT('caason',REST_URL,'apikey','SecretString')
    	geti = geta.response.text
    	getwj = json.loads(geti)
    	for i,j in getwj.items():
            if i == "data":
             	getit = j
    	        context['hi'] = getit
    	RESTC_URL = BASE_URL +'/UCSRestService/report/calldetails/subscriber' 
   	getac = Agent()
	getstartdate = request.POST['startdate']
        enddate =  request.POST['enddate']
	from datetime import datetime
	date_start =    datetime.strptime(getstartdate,'%m/%d/%Y')
        date_end = datetime.strptime(enddate,'%m/%d/%Y') 
        date_start = str(date_start)
	date_end = str(date_end)
	date_start = filter(lambda x: x.isdigit(),date_start)
	date_end = filter(lambda x: x.isdigit(),date_end)
        params['fromDate'] = date_start
	params['toDate'] =   date_end
	params['subscriberId'] = subscriberId
    	getrc = getac.POST(params,'caason',RESTC_URL,'apikey','SecretString')
    	getic = getac.response.text
    	getwjc = json.loads(getic)
    	for k,v in getwjc.items():
           if k == "data":
              context['cdr'] = v
	context['report'] = True
    else:
        REST_URL = BASE_URL +'/UCSRestService/prospectrequest/' + subscriberId + '/subscriber'
        context = {}
        geta = Agent()
        getr = geta.GETT('caason',REST_URL,'apikey','SecretString')
        geti = geta.response.text
        getwj = json.loads(geti)
        for i,j in getwj.items():
            if i == "data":
                getit = j
        context['hi'] = getit

        RESTC_URL = BASE_URL +'/UCSRestService/calldetailrecord/' + subscriberId + '/subscriber'
        getac = Agent()
        getrc = getac.GETT('caason',RESTC_URL,'apikey','SecretString')
        getic = getac.response.text
        getwjc = json.loads(getic)
        for k,v in getwjc.items():
           if k == "data":
              context['cdr'] = v
        context['report'] = True
    return render_to_response('actual/reports.html', context, context_instance=RequestContext(request))


    
def subscriberagent(request):    

    try: request.session['subscriberId']
    except: return HttpResponseRedirect('/subscriber/login/')
    subscriberId = request.session['subscriberId']
    context = {}
    REST_URL = BASE_URL +'/UCSRestService/subscriber/' + subscriberId + '/extensions'
    getclass = RestRequest()
    getr = getclass.GET(REST_URL,'apikey','SecretString')
    gi = getclass.response.text
    getextensions =  json.loads(gi)
    print getextensions
    for k,v in getextensions.items():
        if k == 'data':
            context['extensions'] =v
    context['agents'] = True
    return render_to_response('actual/agents.html', context, context_instance=RequestContext(request))




 
def getagent_detail(request):

    try: request.session['subscriberId']
    except: return HttpResponseRedirect('/subscriber/login/')


    context = {}
    context['agents'] = True
    #customerId =  request.session['customerId']
    subscriberId = request.session['subscriberId']
    REST_URL = BASE_URL +'/UCSRestService/extension'
    if request.method == 'POST':
        params = {}
        get_name = request.POST['name'].encode('utf-8')
        get_username = request.POST['user_name'].encode('utf-8')
        password = request.POST['password'].encode('utf-8')
        get_mobile = request.POST['mobile'].encode('utf-8')
	params['extensionId'] = get_username 
        params['subscriberId']  = subscriberId
        params['customerId']  = 'caason'
        params['name']  = get_name 
	params['userName'] = get_username
	params['password'] = password
        params['contactMobile']  = get_mobile

        params['abnacnNumber']  = 'someabnacnnumbver'
        params['contactPhone1'] = get_mobile#'0399828300'
	params['contactEmail1'] = get_username
	params['contactEmailAddress'] = get_username
	geta = Agent()
        getr = geta.POST(params,'caason',REST_URL,'apikey','SecretString')
	geti = geta.response.text
 	getwj = json.loads(geti)
	print getwj
	print params
        for x,v in getwj.items():
                if x == 'status':
                    if v == 200:
                          message = meeting_template % (get_name,get_username,password,SITE_URL)
                          send_mail(SUBJECT, message,WEBMASTER,(get_username,), fail_silently=False)   

                          return HttpResponseRedirect('/subscriber/agents/')
    return render_to_response('actual/editagent.html', context, context_instance=RequestContext(request))

def add_product(request):
    try: request.session['subscriberId']
    except: return HttpResponseRedirect('/subscriber/login/')
    context = {}
    context['agents'] = True
    #customerId =  request.session['customerId']
    subscriberId = request.session['subscriberId']
    REST_URL = BASE_URL +'/UCSRestService/product'
    if request.method == 'POST':
        params = {}
	RT_URL = BASE_URL +'/UCSRestService/nextproductcode'
        getpa = Agent()
        getpr = getpa.GETT('caason',RT_URL,'apikey','SecretString')
        getpi = getpa.response.text
        getwpj = json.loads(getpi)
	for x,v in getwpj.items():
           if  x == 'data':
              for xx  in v:
                 p_code =  xx['productCode']

        p_name = request.POST['name'].encode('utf-8')
       # p_id = request.POST['productId'].encode('utf-8')
        #p_code = request.POST['productCode'].encode('utf-8')
        p_description= request.POST['description'].encode('utf-8')
	params['name'] = p_name
        params['productCode'] = p_code 
	params['extensionId'] = str(request.session['defaultExtensionId'])
 	#params['productId'] = p_id
	params['description'] = p_description
        geta = Agent()
	print params
	print "++++++++++++++++++++++++++++++"
        getr = geta.POST(params,'caason',REST_URL,'apikey','SecretString')
        geti = geta.response.text
        getwj = json.loads(geti)
        print getwj
        for x,v in getwj.items():
                if x == 'status':
                    if v == 200:
                          return HttpResponseRedirect('/subscriber/products/')

    return render_to_response('actual/addproduct.html', context, context_instance=RequestContext(request))



    
def subscriber_product(request):
    try: request.session['subscriberId']
    except: return HttpResponseRedirect('/subscriber/login/')
    subscriberId = request.session['subscriberId']
    context = {}
    RESTP_URL = BASE_URL +'/UCSRestService/subscriber/' + subscriberId + '/products'
    getclass = RestRequest()
    getr = getclass.GET(RESTP_URL,'apikey','SecretString')
    gi = getclass.response.text
    getproducts =  json.loads(gi)
    print getproducts
    for k,v in getproducts.items():
        if k == 'data':
            context['products'] =v
            for xx in v:
                getextensionid  = xx['extensionId']
                RESTE_URL = BASE_URL +'/UCSRestService/extension/' +str(getextensionid)
                getclasss = RestRequest()
                getrr = getclasss.GET(RESTE_URL,'apikey','SecretString')
                gii = getclasss.response.text
                getextension=  json.loads(gii)
                for k,v in getextension.items():
                   if k == 'data':
                      context['getextension'] = v        
    REST_URL = BASE_URL +'/UCSRestService/subscriber/' + subscriberId + '/extensions'
    getclass = RestRequest()
    getr = getclass.GET(REST_URL,'apikey','SecretString')
    gi = getclass.response.text
    getextensions =  json.loads(gi)
    for k,v in getextensions.items():
        if k == 'data':
            context['extensions'] =v
    context['product'] = True
    return render_to_response('actual/products.html', context, context_instance=RequestContext(request))
         
         
def subscriber_toolkit(request):         
    try: request.session['subscriberId']
    except: return HttpResponseRedirect('/subscriber/login/')
    subscriberId = request.session['subscriberId']
    context = {}
    RESTP_URL = BASE_URL +'/UCSRestService/subscriber/' + subscriberId + '/products'
    getclass = RestRequest()
    getr = getclass.GET(RESTP_URL,'apikey','SecretString')
    gi = getclass.response.text
    getproducts =  json.loads(gi)
    for k,v in getproducts.items():
        if k == 'data':
            context['products'] =v
    context['toolkit'] = True
    return render_to_response('actual/toolkit.html', context, context_instance=RequestContext(request))

def subscriber_print(request):
    context = {}
    try: request.session['subscriberId']
    except: return HttpResponseRedirect('/subscriber/login/')
    context = {}
    subscriberId = request.session['subscriberId']
    REST_URL = BASE_URL +'/UCSRestService/subscriber/' + subscriberId
    getclass = RestRequest()
    getr = getclass.GET(REST_URL,'apikey','SecretString')
    gi = getclass.response.text
    getdict = json.loads(gi)
    for x,v in getdict.items():
        if x == 'data':
            print v
            for xx  in v:
               context['subscribername'] = v

    REST_URL = BASE_URL +'/UCSRestService/subscriber/' + subscriberId + '/extensions'
    getclass = RestRequest()
    getr = getclass.GET(REST_URL,'apikey','SecretString')
    gi = getclass.response.text
    getextensions =  json.loads(gi)
    for k,v in getextensions.items():
        if k == 'data':
            context['extensions'] =v

    RESTP_URL = BASE_URL +'/UCSRestService/subscriber/' + subscriberId + '/products'
    getclass = RestRequest()
    getr = getclass.GET(RESTP_URL,'apikey','SecretString')
    gi = getclass.response.text
    getproducts =  json.loads(gi)
    for k,v in getproducts.items():
            if  k == 'data':
               context['products'] =v


    return render_to_response('actual/print.html', context, context_instance=RequestContext(request))





import os
import zipfile
import StringIO
from django.http import HttpResponse
import shutil

def getfiles(request):
    try: request.session['subscriberId']
    except: return HttpResponseRedirect('/subscriber/login/')


    filenames = []
    filenamess = ["/home/provising/smstext.txt", "/home/provising/calllink.html","/home/provising/smsimage.png","/home/provising/callimage.png","/home/provising/calltext.txt","/home/provising/callbutton.html"]
    count=1
    getpid = request.GET['productCode']
    RESTP_URL = BASE_URL +'/UCSRestService/product/' +getpid + '/smstext'
    getclass = Agent()
    getr = getclass.GETT('caason',RESTP_URL,'apikey','SecretString')
    f = open('/home/provising/smstext.txt','w')
    f.write(getclass.response.text)
    f.close()


    RESTC_URL = BASE_URL +'/UCSRestService/product/' +getpid + '/calllink'
    getclass = Agent()
    getr = getclass.GETT('caason',RESTC_URL,'apikey','SecretString')
    f = open('/home/provising/calllink.html','w')
    f.write(getclass.response.text)
    f.close()

    RESTI_URL = BASE_URL +'/UCSRestService/product/' +getpid + '/smsimage'
    getclass = Agent()
    params = { "foreground" : "#000000", "background" : "#ffffff", "height":"100", "width":"100"}
    getclass = Agent()
    getr = getclass.POST2GetImage(params, 'caason',RESTI_URL,'apikey','SecretString')
    f = open('/home/provising/smsimage.png','w')
    f.write(getr.content)
    f.close()

    RESTC_URL = 'http://10.10.10.76:8100/UCSRestService/product/' +getpid + '/callimage'
    params = { "foreground" : "#000000", "background" : "#ffffff", "height":"100", "width":"100"}
    getclass = Agent()
    getr = getclass.POST2GetImage(params, 'caason',RESTC_URL,'apikey','SecretString')
    f = open('/home/provising/callimage.png','w')
    f.write(getr.content)
    f.close()

    RESTT_URL = BASE_URL +'/UCSRestService/product/' +getpid + '/calltext'
    getclass = Agent()
    getr = getclass.GETT('caason',RESTT_URL,'apikey','SecretString')
    f = open('/home/provising/calltext.txt','w')
    f.write(getclass.response.text)
    f.close()

    RESTB_URL = BASE_URL +'/UCSRestService/product/' +getpid + '/callbutton'
    getclass = Agent()
    getr = getclass.GETT('caason',RESTB_URL,'apikey','SecretString')
    f = open('/home/provising/callbutton.html','w')
    f.write(getclass.response.text)
    f.close()




    shutil.copy2('/home/provising/smstext.txt','/tmp/smstext.txt')
    filenames.append('/tmp/smstext.txt')

    shutil.copy2('/home/provising/calllink.html','/tmp/calllink.html')
    filenames.append('/tmp/calllink.html')

    shutil.copy2('/home/provising/smsimage.png','/tmp/smsimage.png')
    filenames.append('/tmp/smsimage.png')

    shutil.copy2('/home/provising/callimage.png','/tmp/callimage.png')
    filenames.append('/tmp/callimage.png')

    shutil.copy2('/home/provising/callimage.png','/tmp/callimage.png')
    filenames.append('/tmp/callimage.png')

    shutil.copy2('/home/provising/calltext.txt','/tmp/calltext.txt')
    filenames.append('/tmp/calltext.txt')

    shutil.copy2('/home/provising/callbutton.html','/tmp/callbutton.html')
    filenames.append('/tmp/callbutton.html')


    zip_subdir = "Toolkit_" + str(getpid)
    zip_filename = "%s.zip" % zip_subdir
    s = StringIO.StringIO()
    zf = zipfile.ZipFile(s, "w")
    for fpath in filenames:
        fdir, fname = os.path.split(fpath)
        zip_path = os.path.join(zip_subdir, fname)
        zf.write(fpath, zip_path)
    zf.close()
    resp = HttpResponse(s.getvalue(), mimetype = "application/x-zip-compressed")
    resp['Content-Disposition'] = 'attachment; filename=%s' % zip_filename

    return resp
         
